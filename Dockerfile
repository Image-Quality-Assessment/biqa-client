FROM hensansi/angular2
WORKDIR /var/www/angular/

COPY nginx.conf /etc/nginx/nginx.conf

COPY index.html package.json typings.json tsconfig.json systemjs.config.js styles.css ./

COPY app ./app

RUN npm install

EXPOSE 80

