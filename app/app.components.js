"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ng2_file_upload_1 = require("ng2-file-upload/ng2-file-upload");
var AppComponent = (function () {
    function AppComponent() {
        this.file_srcs = [];
        this.option = { isHTML5: true, allowedFileType: ['jpg', 'png'] };
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        console.log(location.host);
        if (location.host == "localhost:3000") {
            this.url = "http://localhost:8082/image-quality-assessment";
        }
        else {
            this.url = 'image-quality-assessment';
        }
        this.uploader = new ng2_file_upload_1.FileUploader({ url: this.url, options: this.option });
        this.uploader.onAfterAddingFile = function (item) {
            // Create an img element and add the image file data to it
            var img = document.createElement("img");
            img.src = window.URL.createObjectURL(item._file);
            // Create a FileReader
            var reader, target;
            var reader = new FileReader();
            // Add an event listener to deal with the file when the reader is complete
            reader.addEventListener("load", function (event) {
                // Get the event.target.result from the reader (base64 of the image)
                img.src = event.target.result;
                // Resize the image
                //var resized_img = this.resize(img);
                // Push the img src (base64 string) into our array that we display in our html template
                item.src = img.src;
            }, false);
        };
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            var res = JSON.parse(response);
            item.algorithms = res.algorithms;
            item.average = res.average;
            item.assessment = res;
        };
    };
    AppComponent.prototype.writeAlgorithmSummary = function (algorithm) {
        console.log(algorithm);
        if (algorithm.success) {
            return algorithm.name + ": " + algorithm.score;
        }
        else {
            return algorithm.name + " failed";
        }
    };
    AppComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: "app/app.component.html",
            styleUrls: ["app/app.component.css"],
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.components.js.map