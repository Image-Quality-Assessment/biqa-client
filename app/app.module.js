"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var platform_browser_1 = require("@angular/platform-browser");
var app_components_1 = require("./app.components");
var file_select_directive_1 = require("ng2-file-upload/components/file-upload/file-select.directive");
var file_drop_directive_1 = require("ng2-file-upload/components/file-upload/file-drop.directive");
var image_preview_directive_1 = require("./image-preview.directive");
var score_component_1 = require("./score.component");
var ng2_accordion_1 = require("ng2-accordion");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, common_1.CommonModule, ng2_accordion_1.AccordionModule],
            declarations: [app_components_1.AppComponent, score_component_1.ScoreComponent, image_preview_directive_1.ImagePreview, file_select_directive_1.FileSelectDirective, file_drop_directive_1.FileDropDirective],
            bootstrap: [app_components_1.AppComponent],
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map