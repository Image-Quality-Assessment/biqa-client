"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Assessment_1 = require("./Model/Assessment");
var ScoreComponent = (function () {
    function ScoreComponent() {
    }
    ScoreComponent.prototype.ngOnInit = function () {
        if (this.assessment) {
            this.hasStddev = true;
            this.indicatorPositionPercent = this.assessment.average * 100;
            this.stddevWidthPercent = this.assessment.standardDeviation * 100;
            this.stddevPositionPercent = this.indicatorPositionPercent - (this.stddevWidthPercent / 2);
        }
        else {
            this.hasStddev = false;
            this.indicatorPositionPercent = this.score * 100;
        }
    };
    ScoreComponent.prototype.setIndicatorPosition = function () {
        return this.indicatorPositionPercent + "%";
    };
    ScoreComponent.prototype.setStddevWidth = function () {
        if (this.hasStddev) {
            return this.stddevWidthPercent + "%";
        }
        else {
            return "0%";
        }
    };
    ScoreComponent.prototype.setStddevPosition = function () {
        if (this.hasStddev) {
            return this.stddevPositionPercent + "%";
        }
        else {
            return "0%";
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Assessment_1.Assessment)
    ], ScoreComponent.prototype, "assessment", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], ScoreComponent.prototype, "score", void 0);
    ScoreComponent = __decorate([
        core_1.Component({
            selector: 'my-score',
            styleUrls: ["app/score.component.css"],
            templateUrl: "app/score.component.html",
            inputs: ['assessment:Assessment']
        }), 
        __metadata('design:paramtypes', [])
    ], ScoreComponent);
    return ScoreComponent;
}());
exports.ScoreComponent = ScoreComponent;
//# sourceMappingURL=score.component.js.map