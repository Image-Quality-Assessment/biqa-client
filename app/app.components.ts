import {Component, OnInit, NgModule} from "@angular/core";
import {FileUploader, FileUploaderOptions} from "ng2-file-upload/ng2-file-upload";
import {Assessment} from "./Model/Assessment";
import {Algorithm} from "./Model/Algorithm";
import {AccordionModule} from "ng2-accordion";



@Component({
    selector: 'my-app',
    templateUrl: "app/app.component.html",
    styleUrls: ["app/app.component.css"],
})
export class AppComponent implements OnInit {
    private url: string;
    file_srcs: string[] = [];
    public option: FileUploaderOptions = {isHTML5: true, allowedFileType: ['jpg', 'png']};
    files: any[];
    public uploader: FileUploader;
    public list: any[];
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;


    ngOnInit() {
        console.log(location.host);
        if (location.host == "localhost:3000") {
            this.url = "http://localhost:8082/image-quality-assessment";
        } else {
            this.url = 'image-quality-assessment';
        }

        this.uploader = new FileUploader({url: this.url, options: this.option});

        this.uploader.onAfterAddingFile = (item: any) => {
            // Create an img element and add the image file data to it
            var img = document.createElement("img");
            img.src = window.URL.createObjectURL(item._file);

            // Create a FileReader
            var reader: any, target: EventTarget;
            var reader: any = new FileReader();

            // Add an event listener to deal with the file when the reader is complete
            reader.addEventListener("load", (event) => {
                // Get the event.target.result from the reader (base64 of the image)
                img.src = event.target.result;

                // Resize the image
                //var resized_img = this.resize(img);

                // Push the img src (base64 string) into our array that we display in our html template
                item.src = img.src;
            }, false);
        };

        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            var res: Assessment = JSON.parse(response);
            item.algorithms = res.algorithms;
            item.average = res.average;
            item.assessment = res;
        };
    }

    writeAlgorithmSummary(algorithm: Algorithm): string {
        console.log(algorithm);
        if (algorithm.success) {
            return algorithm.name + ": " + algorithm.score;
        } else {
            return algorithm.name + " failed"
        }
    }


    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }


}