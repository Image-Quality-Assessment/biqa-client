import {Component, OnInit, Input} from "@angular/core";
import {Assessment} from "./Model/Assessment";

@Component({
    selector: 'my-score',
    styleUrls: ["app/score.component.css"],
    templateUrl: "app/score.component.html",
    inputs: ['assessment:Assessment']

})
export class ScoreComponent implements OnInit {
    @Input() assessment: Assessment;
    @Input() score: number;
    // @Input() myAveragePosition;

    private indicatorPositionPercent;
    private stddevWidthPercent;
    private stddevPositionPercent;

    private hasStddev: boolean;

    ngOnInit(): void {
        if (this.assessment) {
            this.hasStddev = true;
            this.indicatorPositionPercent = this.assessment.average * 100;
            this.stddevWidthPercent = this.assessment.standardDeviation * 100;
            this.stddevPositionPercent = this.indicatorPositionPercent - (this.stddevWidthPercent / 2);
        } else {
            this.hasStddev = false;
            this.indicatorPositionPercent = this.score * 100;
        }
    }

    setIndicatorPosition(): string {
        return this.indicatorPositionPercent + "%";

    }

    setStddevWidth(): string {
        if (this.hasStddev) {
            return this.stddevWidthPercent + "%";
        } else {
            return "0%";
        }

    }

    setStddevPosition(): string {
        if (this.hasStddev) {
            return this.stddevPositionPercent + "%";
        } else {
            return "0%";
        }
    }
}