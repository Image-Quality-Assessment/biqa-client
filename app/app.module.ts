import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.components";
import {FileSelectDirective} from "ng2-file-upload/components/file-upload/file-select.directive";
import {FileDropDirective} from "ng2-file-upload/components/file-upload/file-drop.directive";
import {ImagePreview} from "./image-preview.directive";
import {ScoreComponent} from "./score.component";
import {AccordionModule} from "ng2-accordion";

@NgModule({
    imports: [BrowserModule, CommonModule, AccordionModule],
    declarations: [AppComponent, ScoreComponent, ImagePreview, FileSelectDirective, FileDropDirective],
    bootstrap: [AppComponent],
})
export class AppModule {
}
