export class Assessment {
    "id": number;
    "standardDeviation": number;
    "average": number;
    "algorithms": Algorithm[];
}
