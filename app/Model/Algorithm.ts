/**
 * Created by milan on 10.10.16.
 */
export class Algorithm {
    "description": string;
    "name":string;
    "score": number;
    "success": boolean;
}
